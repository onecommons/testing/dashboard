# export TOKEN=
export DEPLOY_PATH=environments/staging/deployment1
export DEPLOY_ENVIRONMENT=staging
export BLUEPRINT_PROJECT_URL=https://gitlab.com/onecommons/testing/apostrophe-demo.git
export PROJECT_ID=33855663
curl -X POST -F ref=main -F token=$TOKEN -F "variables[DEPLOY_PATH]=$DEPLOY_PATH" -F "variables[DEPLOY_ENVIRONMENT]=$DEPLOY_ENVIRONMENT" -F "variables[BLUEPRINT_PROJECT_URL]=$BLUEPRINT_PROJECT_URL" https://gitlab.com/api/v4/projects/$PROJECT_ID/trigger/pipeline
